Proyecto Final para la materia de Sistemas Inteligentes (2 - 2020).
El proyecto es un 3 en raya en lenguaje Kotlin (Desarrollo movil - Android)

## Some screenshots

<details>
<summary>Click to expand!</summary>
  ![preview1](/assets/01.jpg)
  ![preview2](/assets/02.jpg)
  ![preview3](/assets/03.jpg)
  ![preview4](/assets/04.jpg)
</details>
